
/* global L, distance */

// seznam z markerji na mapi
var markerji = [];
var mapa;
//var obmocje;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

// (moje)
const Y1 = 14.5247825;
const X1 = 46.0536077;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {
  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    
    // samo za testiranje! (moje)
    // center: [Y1, X1],
    
    zoom: 12
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);
  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

/*
  Ročno dodamo fakulteto za računalništvo in informatiko na mapo
  dodajMarker(FRI_LAT, FRI_LNG, "FAKULTETA ZA RAČUNALNIŠTVO IN INFORMATIKO", "FRI");
*/
  // Objekt oblačka markerja
  var popup = L.popup();

  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    popup
      .setLatLng(latlng)
      .setContent("Izbrana točka:" + latlng.toString())
      .openOn(mapa);
      
      // Doda marker tja kamor kliknes (moje)
      var lat = latlng.lat;
      var lng = latlng.lng;
      dodajMarker(lat, lng, "Izbrana točka. ");
      
      /**
       * DODAŠ ŠE DA SE NAJBLIŽJI POLIGONI OBARVAJO ZELENO
       * ostali so modri
       **/
       
  }
  mapa.on('click', obKlikuNaMapo);
  
  // POLIGONI POISKUSANJE
  
  
  var tabela = [];
  jQuery.getJSON("knjiznice/json/bolnisnice.json", function(data){
    for (var i = 0; i < data.features.length; i++) {
      if(data.features[i].geometry.coordinates[0].length > 2){
        tabela = data.features[i].geometry.coordinates[0];
      }
      for (var j = 0; j < tabela.length; j++) {
        var temp = tabela[j][1];
        tabela[j][1] = tabela[j][0];
        tabela[j][0] = temp;
      }
      var poly = L.polygon(tabela, {color: "#228B22", weight: 1});
      poly.addTo(mapa);
      console.log(tabela)
    }
  });
  
});

function dodajMarker(lat, lng, opis, tip) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 'marker-icon-2x-' + (tip == 'FRI' ? 'red' : 'blue') + '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  // in barvo ikone, glede na tip
  var marker = L.marker([lat, lng], {icon: ikona});
  
  // Izpišemo želeno sporočilo v oblaček
  marker.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();

  // Dodamo točko na mapo in v seznam
  marker.addTo(mapa);
  markerji.push(marker);
}